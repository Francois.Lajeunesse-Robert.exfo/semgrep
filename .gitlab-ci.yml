variables:
  GO_VERSION: "1.22.3"
  REPORT_FILENAME: gl-sast-report.json
  MAX_IMAGE_SIZE_MB: 200
  MAX_IMAGE_SIZE_MB_FIPS: 1090
  DS_EXCLUDED_ANALYZERS: "gemnasium-maven,gemnasium-python"
  SAST_EXCLUDED_ANALYZERS: "bandit,eslint,flawfinder,nodejs-scan,phpcs-security-audit,security-code-scan,spotbugs"
  SAST_EXCLUDED_PATHS: "qa, spec, test, tests, tmp, testdata"
  SEARCH_MAX_DEPTH: 20

include:
  - remote: https://gitlab.com/gitlab-org/security-products/ci-templates/raw/master/includes-dev/analyzer.yml
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/upsert-git-tag.yml'

semgrep-meta-rules:
  stage: test
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --config p/semgrep-rule-lints
  allow_failure: true

semgrep-rules-yaml-validation:
  stage: test
  image:
    name: $TMP_IMAGE$IMAGE_TAG_SUFFIX
  script: 
    - ./scripts/validate-rules.sh /rules

integration-test:
  image:
    name: registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable
  services:
    - docker:20.10-dind
  variables:
    TMP_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA$IMAGE_TAG_SUFFIX
  script:
    - rspec -f d
  artifacts:
    when: always
    paths:
      - tmp/**/gl-sast-report.json
    expire_in: 1 week

integration-test-fips:
  extends: integration-test
  variables:
    IMAGE_TAG_SUFFIX: "-fips"
    PRIVILEGED: 'true'

.qa-downstream-sast:
  variables:
    DS_DEFAULT_ANALYZERS: ""
    SAST_EXCLUDED_ANALYZERS: "bandit,eslint"
    SAST_EXCLUDED_PATHS: "" # TEMP: until https://gitlab.com/gitlab-org/gitlab/-/issues/223283

.bap-require-changes:
  rules:
    - if: $BAP_DISABLED
      when: never
    - if: $DOWNSTREAM_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes:
        compare_to: 'refs/heads/main'
        paths:
          - Dockerfile
          - Dockerfile.fips
          - analyze.go
          - qa/bap/work.json
          - qa/bap/verify.sh
          - qa/bap/verify.go

bap-analysis-generate-work:
  stage: performance-metrics
  image: alpine:3.18.4
  extends: .bap-require-changes
  variables:
    TARGET_TAG: $CI_COMMIT_SHA$IMAGE_TAG_SUFFIX
  before_script:
    - apk add --no-cache jq curl sed
  script:
    - echo "UPSTREAM_JOB_ID=$CI_JOB_ID" >> build.env
    - cp qa/bap/work.json work.json
    - |
      export SOURCE_TAG=$(curl -sS -L "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/permalink/latest" | jq '.name' | tr -d 'v\"')
    - sed -i s/SOURCE_TAG/$SOURCE_TAG/g work.json
    - sed -i s/TARGET_TAG/$TARGET_TAG/g work.json
  artifacts:
    paths:
      - work.json
    reports:
      dotenv: build.env

bap-analysis-trigger-analysis:
  stage: performance-metrics
  extends: .bap-require-changes
  needs:
    - job: bap-analysis-generate-work
      artifacts: true
  variables:
    UPSTREAM_PROJECT_ID: $CI_PROJECT_ID
    UPSTREAM_JOB_ID: $UPSTREAM_JOB_ID
    BAP_COMPARE: "true"
  trigger:
    project: gitlab-org/secure/tools/bap
    strategy: depend

bap-analysis-verify-analysis:
  stage: performance-metrics
  extends: .bap-require-changes
  image: alpine:3.18.4 
  needs:
    - bap-analysis-trigger-analysis
  variables:
    DOWNSTREAM_PROJECT_ID: "50773167" # BAP project
  before_script:
    - apk add --no-cache jq git curl make musl-dev go zip bash
  script:
    - ./qa/bap/verify.sh
  artifacts:
    when: always
    paths:
      - ./scan-reports/gl-bap-analysis-report.json
      - ./scan-reports/gl-bap-analysis-report.html
