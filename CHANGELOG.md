Semgrep analyzer changelog

## v5.3.3
- Fix issue where remote rulesets are being ignored (!447)
- Update `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.9` => [`v3.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.0.2)] (!447)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.4.0` => [`v3.0.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v3.0.0)] (!477)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.4.0` => [`v5.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.1.0)] (!477)

## v5.3.2
- update `go` version to `v1.22.3` for both FIPS and Non-FIPS docker images (!442)

## v5.3.1
- Fix `SAST_SCANNER_ALLOWED_CLI_OPTS` to ignore rogue spaces (!444)

## v5.3.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.5.5) version 2.5.5 (!438)
  - Add `rules/lgpl-cc/java/traversal/rule-RelativePathTraversal`
  - Add `rules/lgpl-cc/java/xxe/rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing` 
  - Add `rules/lgpl-cc/java/inject/rule-DangerousGroovyShell` 
  - Add `rules/lgpl-cc/java/inject/rule-MongodbNoSQLi.java` 
  - Add `rules/gitlab/javascript/crypto/rule-NodeLibcurlSSLVerificationDisable` 
  - Add `rules/lgpl-cc/java/crypto/rule-DisallowOldTLSVersion.java` 
  - Add `rules/lgpl-cc/java/deserialization/rule-SnakeYamlConstructor.java`
  - Add `rules/lgpl-cc/java/xxe/rule-DisallowDoctypeDeclFalse.java`
  - Add `rules/lgpl-cc/python/flask/security/injection/path-traversal/rule-path-traversal-open`
  - Add `rules/lgpl-cc/java/crypto/rule-UseOfRC4`
  - Add and update missing mappings for rules
  - Add missing mapping for `rules/lgpl-cc/python/flask/security/injection/path-traversal/rule-path-traversal-open`
  - Add `rules/lgpl-cc/java/inject/rule-EnvInjection.yml` 
  - Add `rules/lgpl-cc/python/crypto/rule-HTTPConnectionPool.yml` 
  - Add `rules/lgpl-cc/python/flask/security/redirection/rule-flask-open-redirect.yml` 
  - Add `java/crypto/rule-WeakTLSProtocolSSLContext.yml` 
  - Add `rules/lgpl-cc/java/crypto/rule-HttpComponentsRequest.yml` 
  - Remove `java/cookie/rule-CookieHTTPOnly` and add `rules/lgpl-cc/java/cookie/rule-CookieHTTPOnly` with enhanced patterns
  - Remove `java/xxe/rule-XMLStreamRdr` and add `rules/lgpl-cc/java/xxe/rule-XMLStreamRdr` with additional patterns
  - Remove `rules/lgpl/javascript/dos/rule-regex_injection_dos` and enhance `javascript/dos/rule-non-literal-regexp` with additional patterns
  - Remove `java/password/rule-HardcodeKeyEquals.yml` as secret detection should be used instead. 
  - Remove `rules/lgpl-cc/java/password/rule-HardcodeKey.yml` as secret detection should be used instead. 
  - Remove `rules/gitlab/scala/password/rule-HardcodeKey.yml` as secret detection should be used instead. 
  - Remove `rules/gitlab/scala/password/rule-HardcodeKeyEquals.yml` as secret detection should be used instead. 
  - Remove `rules/gitlab/scala/password/rule-HardcodeKeySuspiciousName.yml` as secret detection should be used instead. 
  - Remove `rules/gitlab/scala/password/rule-HardcodeKeySuspiciousValue.yml` as secret detection should be used instead. 
  - Remove `rules/lgpl/javascript/traversal/rule-zip_path_overwrite2.yml` 
  - Remove java/random/rule-PseudoRandom.yml rule (!635)
  - Remove rules/lgpl/kotlin/random/rule-PseudoRandom.yml rule (!635)
  - Remove scala/random/rule-PseudoRandom.yml rule (!635)
  - Update `python/sql/rule-hardcoded-sql-expression.yml` 
  - Update `rules/lgpl-cc/java/deserialization/rule-InsecureJmsDeserialization.yml` 
  - Update `java/crypto/rule-WeakTLSProtocolDefaultHttpClient.yml` 
  - Update `rules/lgpl-cc/java/inject/rule-SqlInjection.yml` 
  - Update `python/requests/rule-request-without-timeout.yml` 
  - Update severity ratings across all Java, Scala, and Kotlin password related rules to match 
  - Update `java/password/rule-ConstantDBPassword.yml` to remove patterns that try to match on password like strings 
  - Update `rules/lgpl/kotlin/password/rule-HardcodePassword.yml` to remove patterns that try to match on password like strings 
  - Update `scala/password/rule-ConstantDBPassword.yml` updated description 
  - Update `scala/password/rule-HardcodePassword.yml` updated description 
  - Update `java/file/rule-FilenameUtils` to enhance patterns and use taint mode
  - Update `rules/lgpl/javascript/xml/rule-node_xpath_injection` to reduce false positives
  - Update `java/random/rule-PseudoRandom` and `rules/lgpl/kotlin/random/rule-PseudoRandom` to enhance patterns
  - Update `rules/lgpl/javascript/traversal/rule-generic_path_traversal` to enhance patterns and use taint mode
  - Update `rules/lgpl/javascript/traversal/rule-zip_path_overwrite` to enhance patterns
  - Update `rules/lgpl/javascript/ssrf/rule-node_ssrf` to enhance patterns and use taint mode
  - Update test cases for rule `rules/lgpl/javascript/xml/rule-node_xpath_injection`

## v5.2.4
- Log URL to `sast-rules` repository used to build analyzer image (!437)

## v5.2.3
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.68.0` => [`1.72.0`](https://github.com/semgrep/semgrep/releases/tag/v1.72.0)] (!417)
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!417)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.2.0` => [`v2.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.4.0)] (!417)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.2` => [`v4.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.4.0)] (!417)

## v5.2.2
- Fix a bug where only a subset of rules was copied over (!424)

## v5.2.1
- Add `.h` and `.hpp` file extensions to `plugin/plugin.go` for scanning header only c/cpp projects (!418)
- Bumping sast-rules version to 2.5.2 which includes a bugfix for a rule that prevented the security report from being ingested (!422)

## v5.2.0
- Remove VET as a post-analyzer (!415)

## v5.1.0
- Support rust extensions *.rs (!414)

## v5.0.0
- Bump to next major version (!405)

## v4.18.2
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.5.1) version 2.5.1 (!412)
  - Add `rules/lgpl-cc/java/crypto/rule-SpringHTTPRequestRestTemplate`
  - Add `rules/lgpl-cc/java/deserialization/rule-ServerDangerousObjectDeserialization`
  - Add `rules/lgpl-cc/java/crypto/rule-SpringFTPRequest`
  - Add `rules/lgpl-cc/java/crypto/rule-UseOfRC2`
  - Update `metadata.security-severity` of all rules 
  - Update `rules/lgpl/javascript/traversal/rule-express_lfr` to no longer match `baseUrl`

## v4.18.1
- Move go module path to v4 (!409)

## v4.18.0
- Add Kotlin support (!391)

## v4.17.0
- Support ruby extension .*.rb (!406)

## v4.16.0
- Support swift and objective-c extensions *.swift, *.m (!404)
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.4.0) version 2.4.0 (!404)
  - Add MobSF rules
  - Add Brakeman rules
  - Add `rules/lgpl-cc/java/crypto/rule-HttpGetHTTPRequest` Java HttpGet HTTP request
  - Add `rules/lgpl-cc/java/crypto/rule-HTTPUrlConnectionHTTPRequest` Java HTTPUrlConnection HTTP Request
  - Add `rules/lgpl-cc/java/crypto/rule-SocketRequestUnsafeProtocols` Java Socket Unsafe Protocols
  - Add `rules/lgpl-cc/java/crypto/rule-TLSUnsafeRenegotiation` Java TLS Unsafe Renegotiation
  - Add `rules/lgpl-cc/java/crypto/rule-UnirestHTTPRequest` Java TLS Unirest
  - Add `rules/lgpl-cc/java/file/rule-FilePathTraversalHttpServlet`
  - Add `rules/lgpl-cc/java/xxe/rule-XMLInputFactoryExternalEntitiesEnabled` java xxe xmlinputfactory
  - Update `java/inject/rule-SqlInjection` with missing patterns
  - Update `rules/lgpl-cc/java/inject/rule-SqlInjection` with more sinks, sanitizers and sources
  - Update `python/exec/rule-exec-used` description
  - Update `python/exec/rule-linux-command-wildcard-injection` to cover both wildcards (* and ?) and simplifies the rule
  - Update `rules/lgpl/javascript/jwt/rule-jwt_express_hardcoded`
  - Update `rules/lgpl/javascript/xss/rule-squirrelly_autoescape` to match code written in ES6

## v4.15.2
- Update `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!403)

## v4.15.1
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.3.1) version 2.3.1 (!399)
  - Add `rules/lgpl-cc/java/endpoint/rule-ManuallyConstructedURLs.yml` to detect unsafely constructed URLs that could lead to SSRF
  - Update `rules/lgpl/javascript/jwt/rule-node_jwt_none_algorithm.yml` to use taint instead of search mode
  - Update `rules/lgpl/javascript/eval/rule-eval_require.js` to use taint instead of search mode
  - Update `rules/lgpl/javascript/crypto/rule-node_sha1.yml` with new patterns for better coverage and with updated metadata
  - Update `rules/lgpl/javascript/ssrf/rule-wkhtmltoimage_ssrf.yml` to use taint instead of search mode
  - Update `rules/lgpl/javascript/traversal/rule-express_lfr.yml` to use taint instead of search mode

## v4.15.0
- Support C++ extensions *.cpp, *.c++, *.cc (!398)
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.3.0) version 2.3.0
  - Update C rules for C++

## v4.14.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.2.0) version 2.2.0 (!397)
  - Add NodeJS Scan rules
  - Replace `java/xpathi/rule-XpathInjection` with `rules/lgpl-cc/java/xpathi/rule-XpathInjection` to find all cases of insecure xpath functions usage

## v4.13.5
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.64.0` => [`1.68.0`](https://github.com/semgrep/semgrep/releases/tag/v1.68.0)] (!395)

## v4.13.4
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.1.3) version 2.1.3 (!393)
  - Add `rules/lgpl-cc/java/xxe/rule-ExternalGeneralEntitiesTrue` to detect Java XXE External General Entities set to true
  - Add `rules/lgpl-cc/java/xxe/rule-TransformerfactoryDTDNotDisabled` to detect Java XXE Transformerfactory DTD Not disabled
  - Add `rules/lgpl-cc/java/inject/rule-SeamLogInjection` to detect expression execution in Seam logging API
  - Add `rules/lgpl-cc/java/xxe/rule-ExternalParameterEntitiesTrue` to detect Java XXE External Parameter Entities set to True
  - Add `rules/lgpl-cc/javascript/exec/rule-child-process` to detect command injection
  - Add `python/jwt/rule-jwt-none-alg` to detect 'none' algorithm in a JWT token
  - Add `rules/lgpl-cc/java/csrf/rule-SpringCSRFDisabled` to find all cases of disabled CSRF in spring security module
  - Update `rules/lgpl-cc/java/xxe/rule-SAXParserFactoryDisallowDoctypeDeclMissing` with upgraded patterns from community rule
  - Update `rules/lgpl/javascript/crypto/rule-node_tls_reject` to cover more vulnerable cases i.e. reduce false negatives
  - Update metadata.category
  - Update `rules/lgpl/javascript/xss/rule-express_xss` to use taint instead of search mode
  - Update `rules/lgpl/javascript/jwt/rule-jwt_exposed_credentials` pattern with regex to match object variables containing the string 'password'
  - Update `rules/lgpl/javascript/xml/rule-node_xpath_injection` by converting it to the taint mode
  - Update `go/crypto/rule-tlsversion`
  - Update `rules/lgpl/javascript/jwt/rule-hardcoded_jwt_secret`
  - Split WeakHostNameVerification into `java/endpoint/rule-X509TrustManager` and `java/endpoint/rule-HostnameVerifier`
  - Split and update `java/inject/rule-FileDisclosure into java/inject/rule-FileDisclosureRequestDispatcher` and `java/inject/rule-FileDisclosureSpringFramework`
  - Remove `javascript/exec/rule-child-process`
  - Remove `rules/lgpl/javascript/dos/rule-express_bodyparser` as vulnerability nolonger exists
  - Remove `rules/lgpl/javascript/crypto/rule-node_curl_ssl_verify_disable` since it's obsolete
  - Remove `rules/lgpl/javascript/xml/rule-xxe_sax` as it's FP prone

## v4.13.3
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.1.2) version 2.1.2 (!386)
  - Add `rules/lgpl-cc/java/crypto/rule-GCMNonceReuse.yml` to detect reuse of cryptographic initialization vector
  - Update `go/injection/rule-ssrf.yml` to use taint instead of search mode and add improved patterns and tests
  - Update `rules/lgpl/javascript/xss/rule-handlebars_safestring.yml` to use taint mode, update metadata and add sanitizer patterns and tests
  - Update `go/sql/rule-concat-sqli.yml` to use taint mode to reduce false-positives
  - Remove duplicate rule `rules/lgpl/javascript/exec/rule-generic_os_command_exec.yml`

## v4.13.2
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.1.1) version 2.1.1 (!384)
  - Update `rules/lgpl/javascript/crypto/rule-node_insecure_random_generator.yml` with better description text and pattern constraints
  - Update `rules/lgpl/javascript/eval/rule-yaml_deserialize.yml` to match on typescript import pattern
  - Update `rules/lgpl/javascript/xss/rule-handlebars-noescape.yml` with improved patterns and test-cases
  - Update `rules/lgpl/javascript/crypto/rule-node_md5.yml` with improved patterns and description text
  - Update `javascript/xss/rule-mustache-escape.yml` to match on how escape is actually used in mustache
  - Remove `rules/lgpl/javascript/xml/rule-xxe_xml2json.yml`
  - Remove all rules under `rules/lgpl/javascript/generic` as they contain secret detection rules or are FP prone
  - Import initial Ruby ruleset (but not yet enabled)
  - Correctly apply license for `rules/lgpl-cc/java/ftp/rule-FTPInsecureTransport` in distribution file
  - Correctly apply license for `rules/lgpl-cc/java/password/rule-HardcodeKey` in distribution file
  - Correctly apply license for `rules/lgpl-cc/java/crypto/rule-JwtNoneAlgorithm` in distribution file

## v4.13.1
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.61.1` => [`1.64.0`](https://github.com/semgrep/semgrep/releases/tag/v1.64.0)] (!382)
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!382)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!382)
- update `go` version to `v1.19` for both FIPS and Non-FIPS docker images (!382)

## v4.13.0
- Adding php support and phpcs-security-audit ruleset (!367)

## v4.12.3
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.14) version 2.0.14 (!377)
  - Add Typescript support to rules/lgpl/javascript/eval/rule-node_deserialize.yml
  - Add Typescript support to rules/lgpl/javascript/eval/rule-serializetojs_deserialize.yml
  - Update rules/lgpl-cc/python/django/security/injection/sql/rule-django-rawsql-used with improved patterns and test-cases
  - Update rules/lgpl-cc/python/django/security/injection/sql/rule-django-rawsql-used.yml with improved patterns
  - Remove java/inject/rule-CustomInjection as patterns have been merged with java/inject/rule-SqlInjection

## v4.12.2
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.13) version 2.0.13 (!376)
  - Fix csharp/xss/rule-HtmlElementXss.yml pattern that was causing false positives
  - Update rules/lgpl/javascript/eval/rule-grpc_insecure_connection.yml to support typescript import pattern
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.12) version 2.0.12 (!376)
  - Add rules/lgpl-cc/java/ftp/rule-FTPInsecureTransport.yml to test for insecure FTP client usage
  - Add rules/lgpl-cc/python/django/security/injection/sql/rule-django-raw-used with improved test-cases
  - Add rules/lgpl-cc/java/crypto/rule-JwtNoneAlgorithm.yml to detect JWT none algorithm usage
  - Add security-severity metadata fields to all rules to allow for finer grained severity levels
  - Split C# XSS rule into two rules csharp/xss/rule-HtmlElementXss.yml and csharp/xss/rule-ScriptXss.yml
  - Split C# XXE rule into two rules csharp/injection/rule-XmlDocumentXXEInjection.yml and csharp/injection/rule-XmlReaderXXEInjection.yml
  - Merge java/inject/rule-CustomInjectionSQLString.yml with java/inject/rule-SqlInjection.yml
  - Update java/inject/rule-SqlInjection.yml to use taint mode
  - Update csharp/injection/rule-LdapInjection.yml with additional sinks
  - Update python/escaping/rule-use-of-mako-templates.yml to check for use of default_filters
  - Update go/injection/rule-ssrf.yml to exclude tests
  - Update go/unsafe/rule-unsafe.yml to fix description text where sentences were incorrectly duplicated
  - Update rules/lgpl-cc/java/password/rule-HardcodeKey.yml with more patterns
  - Update rules/lgpl-cc/java/password/rule-HardcodeKey.yml to apply correct license
  - Update rules/lgpl/javascript/redirect/rule-express_open_redirect.yml to detect more patterns
  - Update rules/lgpl/javascript/redirect/rule-express_open_redirect2.yml to detect more patterns
  - Update rules/lgpl/javascript/xss/rule-xss_serialize_javascript.yml with more applicable patterns
  - Update java/smtp/rule-SmtpClient.yml with better patterns to reduce false positives
  - Remove python/exec/rule-import-subprocess.yml as import subprocess does not equate to a vulnerability
  - Remove go/secrets/rule-secrets.yml as secret detection should be used for detecting secrets

## v4.12.1
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.56.0` => [`1.61.1`](https://github.com/semgrep/semgrep/releases/tag/v1.61.1)] (!374)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!372)

## v4.12.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.11) version 2.0.11 (!370)
  - License GitLab rules as GitLab Enterprise Edition
  - Update `go/filesystem/rule-decompression-bomb.yml` adds io.LimitReader as a sanitizer
  - Update `java/inject/rule-ELInjection.yml` with additional patterns
  - Add `java/crypto/rule-JwtDecodeWithoutVerify.yml` to detect the decoding of a JWT token without a verify step
  - Add OWASP mappings for all C# rules
  - Add OWASP mappings for all Go rules
  - Add OWASP mappings for all Python rules
  - Add OWASP mappings for all Java rules
  - Remove `java/inject/rule-CLRFInjectionLogs.yml` as modern loggers (from at least 2018) no longer allow injection of control characters

## v4.11.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.10) version 2.0.10 (!365)
  - Update `python/eval/rule-eval.yml` to enhance eval detection, Python constant string parsing and reduce false positives
  - Update `java/endpoint/rule-UnvalidatedRedirect.yml` to use taint analysis mode, add sanitizers and rewrite message
  - Update `python/snmp/rule-snmp_weak_cryptography.yml` to add correct patterns
  - Update `python/urlopen/rule-urllib_urlopen.yml` to reduce false positives and exclude hard-coded strings.
  - Update `java/strings/rule-BadHexConversion.yml` to track taint in loops
  - Update `go/file_permissions/rule-fileperm.yml` with more sensible mask permissions
  - Rename `go/filesystem/rule-filereadtaint.yml` to `go/filesystem/rule-fileread.yml` and convert to taint mode to reduce false positives
  - Rename `go/filesystem/rule-dirtraversal.yml` to `go/filesystem/rule-httprootdir.yml` and use update the CWE from CWE-22 to CWE-552
  - Update `go/filesystem/rule-tempfiles.yml` with additional patterns
  - Update `go/filesystem/rule-ziparchive.yml` with additional patterns
  - Merge `go/http/rule-slowloris.yml` into `go/http/rule-http-serve.yml`
  - Update `go/leak/rule-pprof-endpoint.yml` with more applicable patterns
  - Remove `go/memory/rule-math-big-rat.yml` this flaw only affects older Go versions
  - Update `go/network/rule-bind-to-all-interfaces.yml` with a better regex to match all bind interfaces
  - Update `csharp/csrf/rule-Csrf.yml` with additional pattern-not constraints to reduce false positives
  - Add and update OWASP 2017 and OWASP 2021 mappings to all C rules
  - Update `java/cookie/rule-HttpResponseSplitting.yml` with fixed regex to match CR LF characters and add more sources
  - Update `java/file/rule-FileUploadFileName.yml` with better description text and improved patterns
  - Rename `python/ssh/rule-ssl-nohost-key-verification.py` to `python/ssh/rule-ssh-nohost-key-verification.py`
  - Update `csharp/password/rule-PasswordComplexity.yml` to match on the correct password setting values
  - Rename `python/urlopen/rule-urllib-urlopen1.yml` to `python/urlopen/rule-urllib-urlopen.yml` and update with additional patterns

## v4.10.4
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.55.2` => [`1.56.0`](https://github.com/semgrep/semgrep/releases/tag/v1.56.0)] (!363)

## v4.10.3
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.9) version 2.0.8 (!360)
  - Update `java/cookie/rule-CookieHTTPOnly.yml` to support jakarta servlet
  - Removed `java/xss/rule-XSSReqParamToSendError.yml` as sendError is now automatically encoded and this was a bug (CVE-2008-1232) fixed in Apache Tomcat 6 in 2008
  - Update `java/cookie/rule-CookieInsecure.yml` to support jakarta servlet
  - Update `java/xss/rule-WicketXSS.yml` to cover more sinks
  - Update `java/script/rule-ScriptInjection.yml` to match invokeFunction() and invokeMethod() with added sinks and rule out false positives for eval()
  - Update `java/xpathi/rule-XpathInjection.yml` to include taint mode analysis and to add sanitizer for setting custom variable resolver
  - Update `csharp/injection/rule-CommandInjection.yml` to ignore hardcoded strings
  - Update `python/deserialization/rule-pickle.yml` to reduce false positives
  - Add back `java/inject/rule-CustomInjectionSQLString.yml` with more strict patterns for matching possible sql injection strings
  - Update `csharp/other/rule-UnsafeXSLTSettingUsed.yml` by changing CWE-611 to 74, update patterns
  - Update `javascript/eval/rule-eval-with-expression.yml` to add more sinks for eval style injections
  - Update `java/cookie/rule-RequestParamToHeader.yml` to fix regex match on new lines, add more sinks
  - Update `csharp/injection/rule-CommandInjection.yml` to add more patterns to match command injection
  - Update `csharp/endpoint/rule-UnvalidatedRedirect.yml` to add sources and sinks
  - Update `java/xml/rule-SAMLIgnoreComments.yml` to add fully qualified class name

## v4.10.2
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.50.0` => [`1.55.2`](https://github.com/semgrep/semgrep/releases/tag/v1.55.2)] (!359)
  - taint-mode fix
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!359)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!359)

## v4.10.1
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.8) version 2.0.8 (!356)
  - Fix OWASP Top 10 category references and typos in multiple rules
  - Update the existing rule `rules/lgpl/javascript/database/rule-node_sqli_injection.yml` to add support for sequelize, optimize existing patterns and cover more sql cases
  - Update `go/sql/rule-concat-sqli.yml` to cover more cases and merge it with `go/sql/rule-format-string-sqli.yml`
  - Update `go/injection/rule-ssrf.yml` to remove false-positives
  - Update `python/exec/rule-subprocess-popen-shell-true.yml` to remove false-positives
  - Update `python/sql/rule-hardcoded-sql-expression.yml` to remove false-positives
  - Update `java/inject/rule-LDAPInjection.yml` to remove false-positives
  - Update `java/script/rule-SpelExpressionParser.yml` to also match `parseRaw()` injections
  - Update `java/strings/rule-ModifyAfterValidation.yml` to match `replaceAll`, `replaceFirst` & `concat` as possible sinks
  - Rename `java/script/rule-SpelExpressionParser.yml` to `rule-SpringSpelExpressionParser.yml` to avoid naming collision
  - Update `java/cors/rule-PermissiveCORSInjection.yml` with additional sinks
  - Update `java/crypto/rule-RsaNoPadding.yml` to eliminate NoPadding false-positives when RSA is not being used
  - Update `go/filesystem/rule-filereadtaint.yml` to detect more cases

## v4.10.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.2) version 2.0.2 (!353)
  - Remove poor or outdated Java rules
  - Update JavaScript `rule-non-literal-regexp.yml` to filter out usage of RegExp literals
  - Update JavaScript `rule-non-literal-regexp.yml` to match non-constructor `RegExp()` function calls

## v4.9.2
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v2.0.0) version 2.0.0 which now uses package registry for rules (!352)

## v4.9.1
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.45.0` => [`1.50.0`](https://github.com/semgrep/semgrep/releases/tag/v1.50.0)] (!350)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!350)

## v4.9.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.45) version 1.3.45 (!351)
  - Remove poor or outdated C# rules

## v4.8.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.44) version 1.3.44 (!349)
  - Remove poor or outdated JavaScript rules

## v4.7.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.43) version 1.3.43 (!348)
  - Remove poor or outdated Python rules

## v4.6.1
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.42) version 1.3.42 (!347)
  - Improve precision of the C# insecure deserialization rule

## v4.6.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.41) version 1.3.41 (!346)
  - Remove poor or outdated Go rules

## v4.5.0
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.40) version 1.3.40 (!345)
  - Remove poor or outdated C rules

## v4.4.17
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.41.0` => [`1.45.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.45.0)] (!340)

## v4.4.16
- downgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.43.0` => [`1.41``.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.41.0)] to fix performance concerns in Java projects (!337)

## v4.4.15
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.41.0` => [`1.43.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.43.0)] (!332)
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (!332)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (!332)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!332)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (!332)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!332)
- upgrade `golang.org/x/crypto` version [`v0.8.0` => [`v0.14.0`](https://golang.org/x/crypto@v0.14.0)] (!332)

## v4.4.14
- upgrade [`common`](gitlab.com/gitlab-org/security-products/analyzers/common/v3) to [`3.2.3`]((https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)) (!329)
  - Fix trusting Custom CA Certificate for UBI-based images
- Move custom CA bundle file path to trust anchors location in FIPS docker image (!329)
- Upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.24.0` => [`1.41.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.41.0)] (!329)

## v4.4.13
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.4.1`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.4.1) (!330)

## v4.4.12
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.39) version 1.3.39 (!327)
  - Disable SAST `message` field wordwrap and update rules that had incorrectly wrapped URLs.

## v4.4.11
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.30+2) version 1.3.30+2 (!323)
  - Fix $ADDR var bind error in find_sec_bugs_scala.URLCONNECTION_SSRF_FD scala rule
  - Fix $PWD var bind error in find_sec_bugs.HARD_CODE_PASSWORD java rule

## v4.4.10
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.30+1) version 1.3.30+1 (!321)
  - Change rule ID format from find_sec_bugs.XYZ to find_sec_bugs_scala.XYZ for Scala rules

## v4.4.9
- Upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!322)
  - Increase maximum size of a raw or file passthrough to 10MB to accommodate large ruleset files

## v4.4.8
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.30) version 1.3.30 (!314)
  - Enhance Python ruleset descriptions and titles

## v4.4.7
- Upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.3.8`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.3.8) (!307)

## v4.4.6
- Upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!303)
    - Do not fail scan upon SARIF `toolExecutionNotifications` of level error (v4.1.5)
    - Update SARIF parser to use Name over Title (v4.1.4)

## v4.4.5
- Upgrade Tracking Calculator to `v2.3.7` (!302)

## v4.4.4
- Upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!300)
    - Update passthrough support to handle ambiguous/short refs

## v4.4.3
- Upgrade Tracking Calculator to `v2.3.4` (!297)

## v4.4.2
- Upgrade Tracking Calculator to `v2.3.3` (!295)
  - v2.3.2
    - Upgrade `tree-sitter` to `v0.20.8`
    - Upgrade grammar `tree-sitter-c` to `v0.20.2`
    - Upgrade grammar `tree-sitter-c-sharp` to `v0.20.0`
    - Upgrade grammar `tree-sitter-cpp` to `v0.20.0`
    - Upgrade grammar `tree-sitter-java` to `v0.20.1`
    - Upgrade grammar `tree-sitter-kotlin` to `v0.2.11`
    - Upgrade grammar `tree-sitter-python` to `v0.20.0`
  - v2.3.3
    -  feat: Add `-deduplicate` flag for controlling signature deduplication

## v4.4.1
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.27) version `1.3.29` (!294)
  - v1.3.29
    - Improve Go memory aliasing in `G601`
  - v1.3.28
    - Enhance Javascript ruleset descriptions and titles

## v4.4.0
- Upgrade Tracking Calculator to v2.3.1 (!288)

## v4.3.7
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.27) version `1.3.27` (!291)
  - v1.3.27
    - Update Java rule-SSRF.yml to match more cases under java.net.* package
    - Add Java rule rule-WeakTLSProtocolVersion.yml to detect weak TLS versions
  - v1.3.26
    - Update Javascript rule-non_literal_fs_filename.yml to only flag on fs modules

## v4.3.6
- Update upstream scanner's exclusion configuration(`semgrepignore`) (!290)
  - Disable git-aware filtering by removing the `.gitignore` inclusion entry (@SimonGurney)
  - Consider `.semgrepignore` file defined the project for exclusion (@hmrc.colinameigh)

## v4.3.5
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.25) version `1.3.25` (!289)
  - v1.3.25
    - Update Java `rule-SpotbugsPathTraversalAbsolute.yml` to handle getResourceAsStream and getResource
  - v1.3.24
    - Remove `-1` from all eslint rule IDs

## v4.3.4
- Remap `Title` to `Name` field (!287)

## v4.3.3
- Import Bandit ruleset from [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) (!272)

## v4.3.2
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.1` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!283)

## v4.3.1
- Update [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.23) version `1.3.23` (!286)
  - v1.3.23
    - Update Java `rule-CommandInjection.yml` to match concatenated strings
    - Update Java `rule-SpelView.yml` to also match `ExpressionParser` interface methods
    - Update Java `rule-XpathInjection.yml` to match actual XPath import path
  - v1.3.22
    - Update Java `rule-CommandInjection.yml` with ability to match on String arrays
  - v1.3.21
    - Update Java `rule-BlowfishKeySize.yml` to add back missing `metavariable`
    - Update Java rules with minor grammatical fixes
  - v1.3.20
    - Enhance Java ruleset descriptions and titles
  - v1.3.19
    - Update Primary identifiers for `bandit.B303` and `bandit.B304` so that they match the published rules in semgrep
    - Remove `-1` from bandit ruleset IDs and primary identifiers to match the published rules in semgrep

## v4.3.0
- SastBot VET registry access (!243)

## v4.2.8
- Upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.21.0` => [`1.23.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.23.0)] (!276)
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.18) version `1.3.18` (!276)
  - Update rules that were missing titles by moving them to shortDescription instead of cwe [sast-rules!161](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/161)
  - Update Primary identifiers for bandit.B303 and bandit.B304 so that they match the published rules in semgrep [sast-rules!155](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/155)
  - Update Primary identifiers for bandit.B103 so that they match the published rules in semgrep [sast-rules!154](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/154)
  - Update primary identifier of bandit.B108-2 to bandit.B108-1 [sast-rules!153](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/153)

## v4.2.7
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.17.1` => [`1.21.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.21.0)] (!257)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!257)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.1)] (!257)

## v4.2.6
- Import Gosec ruleset from [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) (!263)
- Enhance Go ruleset descriptions and titles [sast-rules!137](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/137)

## v4.2.5
- Fixes custom ruleset loading behavior for remote rulesets using file passthrough (!258)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v2.0.1` => [`v2.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.0.2)] (!258)

## v4.2.4
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.14) version `1.3.14` (!269)
  - Update C# SQL Injection with link for more details [sast-rules!139](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/139)
  - Enhance C# ruleset descriptions and titles [sast-rules!134](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/134)
- Import find_sec_bugs ruleset from [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) (!269)
- Import security_code_scan ruleset from [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) (!269)

## v4.2.3
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.13) version `1.3.13` (!268)
  - feat: Drop high-FP eslint detect-object-injection rule [sast-rules!151](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/151)

## v4.2.2
- TEMP: Use patched version of `sast-rules` to remove detect-object-injection (!266)
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.12) version `1.3.12` (!266)
- chore: Drop high-FP rules removal behavior (!266)

## v4.2.1
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.11) version `1.3.11` (!249)
  - Flawfinder singular rule IDs should include `-1` [sast-rules!147](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/147)
  - Enhance C ruleset descriptions and titles [sast-rules!128](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/128)
  - Add shortDescription titles to C rulesets [sast-rules!128](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/128)
  - Add valdiation to confirm that either cwe tag contains title, or shortDescription is defined [sast-rules!128](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/128)
- Import Flawfinder ruleset from [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) (!249)

## v4.2.0
- Add support for `.scala` and `.sc` file extensions (!254)
- Upgrade [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tags/v1.3.10) version `1.3.10` (!254)
  - Enhance usecase coverage for Scala rules [sast-rules!142](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/142)
  - Remove redundant mapping of find_sec_bugs in Scala mapping [sast-rules!142](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/142)
  - Introduce native_analyzer property in the mappings file and use it for primary ID prefix [sast-rules!142](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/142)
- Import Scala rules from [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) (!254)

## v4.1.2
- Update `eslint.yml` rules to [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) version `1.3.6`
  - Update `metadata.owasp` to adhere to the pattern `A{number}:{year}-{Title}` [sast-rules!136](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/136)

## v4.1.1
- Use updated pattern from avoid-pyyaml-load in B506 (@stevep-arm !255)

## v4.1.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.1` => [`v2.0.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.0.1)] (!245)
- enable support for loading remote ruleset configs (!245)

## v4.0.2
- Fix OWASP naming (!246 @artem-fedorov)

## v4.0.1
- Update `eslint.yml` rules to [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules) version `1.3.0` (!244)
  - Remove security prefix [sast-rules!94](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/94)
  - Subexpression matching [sast-rules!55](https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/55)

## v4.0.0
- Bump to next major version (!239)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.3` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!239)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!239)

## v3.14.8
- construct the JSON report `cve` value from rule primary identifier rather than the rule ID (!241)

## v3.14.7
- fix: Update JSON report `scan.primary_identifiers` to match rule primary identifiers, not rule IDs (!240)

## v3.14.6
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.2` => [`v1.10.3`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.3)] (!238)

## v3.14.5
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.3.0` => [`1.17.1`](https://github.com/returntocorp/semgrep/releases/tag/v1.17.1)] (!235)
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!235)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!235)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.22.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!235)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!235)
- upgrade `golang.org/x/crypto` version [`v0.5.0` => [`v0.8.0`](https://golang.org/x/crypto@v0.8.0)] (!235)
- upgrade `golang.org/x/mod` version [`v0.9.0` => [`v0.10.0`](https://golang.org/x/mod@v0.10.0)] (!235)

## v3.14.4
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report` version [`v3.19.0` => [`v3.22.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.0)] (!231)

## v3.14.3
- bump VET version to fix a crash in the Go front-end that appeared in the presence of placeholders in variable declarations (!227)

## v3.14.2
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!226)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report` version [`v3.18.0` => [`v3.19.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.19.0)] (!226)

## v3.14.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report` version [`v3.17.0` => [`v3.18.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.18.0)] (!221)

## v3.14.0
- Disables high-FP rules when either `SAST_EXPERIMENTAL_FEATURES` is `true` or running after 15.9 release (!187)

## v3.13.4
- Integrate rule refinement for rule B113 (!217)

## v3.13.3
- Integrate rule refinement for rule G301 (!201)

## v3.13.2
- Fix file extension filter for VET (!202)
- Reduce logging output for debugging operations (!202)

## v3.13.1
- Add new rules from upstream secure scanners (!204)
  + ESLint: detect-new-buffer
  + FindSecBugs/SpotBugs : SPRING_CSRF_PROTECTION_DISABLED, SQL_INJECTION, SQL_INJECTION_TURBINE, SQL_INJECTION_HIBERNATE, SQL_INJECTION_VERTX, XSS_REQUEST_PARAMETER_TO_SEND_ERROR, SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING
  + GoSec: G111, G112, G113, G114
  + Bandit: B113, B202, B508, B509, B612, B415

## v3.13.0
- Add `primary-id` and `secondary-ids` to rules metadata (!192)
- Updates the convert function to use the identifiers from the rules in vuln ids (!192)

## v3.12.1
- Disable trimming prefix of the vulnerability's file location in the SAST report (!197)

## v3.12.0
- upgrade Post-analyzer script version to v0.2.0, enabling Go FP reduction by default (!200)

## v3.11.2
- Run Semgrep in verbose mode when SECURE_LOG_LEVEL=debug (!199)

## v3.11.1
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`1.1.0` => [`1.3.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.3.0)] (!193)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!193)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!193)
- upgrade `golang.org/x/crypto` version [`v0.4.0` => [`v0.5.0`](https://golang.org/x/crypto@v0.5.0)] (!193)

## v3.11.0
- Integrate VET for FP reduction on Go files (!189)

## v3.10.1
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`0.121.2` => [`1.1.0`](https://github.com/returntocorp/semgrep/releases/tag/v1.1.0)] (!186)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!186)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!186)
- upgrade `golang.org/x/crypto` version [`v0.2.0` => [`v0.4.0`](https://golang.org/x/crypto@v0.4.0)] (!186)
- update `go` version to `v1.18` for Non-FIPS docker image (!186)

## v3.10.0
- Include `scan.primary_identifiers` (!146)
- Bump tracking-calculator version to v2.2.8 (!146)

## v3.9.4
- Improve scan performance and reduce false positives for Bandit rule: B610  (!183)
- Improve scan performance of Bandit rules: B110, B112 (!183)

## v3.9.3
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!177)
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!177)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!177)
- upgrade `golang.org/x/crypto` version [`v0.0.0-20221012134737-56aed061732a` => [`v0.2.0`](https://golang.org/x/crypto@v0.2.0)] (!177)

## v3.9.2
- Revert: Upgrade base image to ubi9-minimal in FIPS variant of Dockerfile (!180)

## v3.9.1
- Refined B610 to eliminate FP patterns (!179)
- Refined B108 to match the original patterns more accurately (!179)
- Use generic pattern matching for B110, B112 to support exception chains (!179)
- Broke down the single rule B313, B314, B315, B316, B317, B318, B319, B320, B405, B406, B407, B408, B409, B410 into smaller pieces (!179)
- Reduced the rules; the initial rules-set was based on a rule-set provided by r2c where we focused more on coverage than actual parity. We trimmed down some of the rules to match more accurately what bandit would return. (!179)

## v3.9.0
- Upgrade base image to ubi9-minimal in FIPS variant of Dockerfile (!175)

## v3.8.3
- Upgrade gitlab.com/gitlab-org/security-products/analyzers/report to v3.16.0 (!162)
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`0.115.0` => [`0.121.2`](https://github.com/returntocorp/semgrep/releases/tag/v0.121.2)] (!162)
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!162)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!162)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.0` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!162)
- temporarily install C build tools to compile dependencies (!162)

## v3.8.2
- Fix: Stabilize go fixtures, reduce duplication (!171)

## v3.8.1
- Fix FP patterns in SQL Injection and Hardcoded Password in Java (!172)

## v3.8.0
- Populates the `cve` field of each vulnerability finding (!169)

## v3.7.6
- Upgrade gitlab.com/gitlab-org/security-products/analyzers/report to v3.15.5 (!168)

## v3.7.5
- Switch to use ubi8-minimal as the base FIPS image (!165)

## v3.7.4
- Include `scan.analyzer` in JSON report output (!159)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.15.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.3)] (!159)
- Bump tracking-calculator version to v2.2.6 (!159)

## v3.7.3
- Bump go-fips builder image to 1.18 (!161)

## v3.7.2
- Upgrade to the latest security-code-scan ruleset 1.0.67 (!156)

## v3.7.1
- Include missing security-code-scan identifiers (!152)

## v3.7.0
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`0.110.0` => [`0.115.0`](https://github.com/returntocorp/semgrep/releases/tag/v0.115.0)] (!149)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.2` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!149)

## v3.6.1
- Upgrading to the latest find-sec-bugs rule-set 1.0.63 (!153)

## v3.6.0
- Adding Csharp support and security-code-scan ruleset (!137)

## v3.5.0
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`0.104.0` => [`0.110.0`](https://github.com/returntocorp/semgrep/releases/tag/v0.110.0)] (!140)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!140)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!140)

## v3.4.0
- Upgrade the `command` package (!141)
  + Logs environment variables at a debug level before running

## v3.3.2
- Add support `SAST_SCANNER_ALLOWED_CLI_OPTS` CI variable (!139)
- Add `--max-memory` flag under list of scanner allowed CLI options (!139)

## v3.3.1
- Upgrade the `command` package for better analyzer messages. (!138)

## v3.3.0
- Disables high-FP rules when `SAST_EXPERIMENTAL_FEATURES` is `true` (!135)

## v3.2.1
- upgrade [`Semgrep`](https://github.com/returntocorp/semgrep) version [`0.98.0` => [`0.104.0`](https://github.com/returntocorp/semgrep/releases/tag/v0.104.0)] (!133)
- upgrade `github.com/stretchr/testify` version [`v1.7.0` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!133)
- upgrade `github.com/urfave/cli/v2` version [`v2.3.0` => [`v2.11.0`](https://github.com/urfave/cli/releases/tag/v2.11.0)] (!133)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.11.0` => [`v3.12.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.12.2)] (!133)

## v3.2.0
- Upgrade the `common` and `command` packages, and remove no-op commands from the Dockerfile (!130)
  + Adds support for globstar patterns.
  + Resolves an issue using custom CA certs with the FIPS image
  + Removes commands from the Dockerfile that set up the `semgrep` user and assigns permissions to the `.pem` cert bundle, as they didn't have an effect

## v3.1.1
- Bypass language-based matching filter when custom ruleset contains Semgrep rules (!127)

## v3.1.0
- Bump semgrep version to [0.98.0](https://github.com/returntocorp/semgrep/releases/tag/v0.98.0) (!120)
    + Fixed: Fixed a non-deterministic crash when matching a large number of regexes
    + Changed: If a metrics event request times out, we no longer retry the request. This avoids Semgrep waiting 10-20 seconds before exiting if these requests are slow.
    + Changed: The metrics collection timeout has been raised from 2 seconds to 3 seconds.
    + Fixed: Handle utf-8 decoding errors without crashing
    + Changed: Parsing: fail fast on in semgrep-core if rules fail to validate

## v3.0.3
- Trim whitespace from values in `SAST_EXCLUDED_PATHS` (!123)
- Match `SAST_EXCLUDED_PATHS` values relative to the project root (!124)

## v3.0.2
- Fix dependencies conflict resolution via `--best --allowerasing` yum flags (!122)
- Remove redundant `yum update` operation in favor of `yum upgrade` (!122)

## v3.0.1
- Bumping gosec ruleset to v1.0.47 (!119)
    - Remove incorrect mapping to Gosec rule G104, which is not currently implemented
    - Add rule G402 to detect TLS versions before 1.2

## v3.0.0
- Bumping to `v3.0.0` (!114)

## v2.21.0
- Adding Java support (!111)

## v2.20.2
- Add custom CA support for FIPS docker image (!112)

## v2.20.1
- Add `Dockerfile.fips` and include FIPS image releases (!108)

## v2.20.0
- Update semgrep to [0.86.5](https://github.com/returntocorp/semgrep/releases/tag/v0.86.5) (!109)
    + 0.86.2 Notable Changes
      + Fixed: Some finding fingerprints were not matching what semgrep-agent would return.
    + 0.86.1 Notable Changes
      + Changed: --timeout-threshold default set to 3 instead of 0
    + 0.86.0 Notable Changes
      + Added: Go: use latest tree-sitter-go with support for Go 1.18 generics
      + Changed: Findings are now considered identical between baseline and current scans based on the same logic as Semgrep CI uses, which means:
        + Two findings are now identical after whitespace changes such as re-indentation
        + Two findings are now identical after a nosemgrep comment is added
      + Changed: Findings are now different if the same code triggered them on different lines
      + Fixed: Symlinks found in directories are skipped from being scanned again. This is a fix for a regression introduced in 0.85.0.
      + Fixed: Go: fix unicode parsing bugs by switching to latest tree-sitter-go
      + Fixed: Constant propagation: A conditional expression where both alternatives are constant will also be considered constant
      + Fixed: Constant propagation now recognizes operators ++ and -- as side-effectful
    + 0.85.0 Notable Changes
      + Added: When scanning multiple languages, Semgrep will now print a table of how many rules and files are used for each language.
      + Fixed: Fixed Deep expression matching and metavariables interaction. Semgrep will not stop anymore at the first match and will enumarate all possible matchings if a metavariable is used in a deep expression pattern (e.g., <... $X ...>). This can introduce some performance regressions.
      + Fixed: JSX: ellipsis in JSX body (e.g., <div>...</div>) now matches any children (#4678 and #4717)
      + Changed: When git ls-files is unavailable or --disable-git-ignore is set, Semgrep walks the file system to find all target files. Semgrep now walks the file system 30% faster compared to previous versions.

## v2.19.1
- Fixed Docker build issue (!107)

## v2.19.0 (Unreleased due to build issues)
- Update semgrep to [0.84.0](https://github.com/returntocorp/semgrep/releases/tag/v0.84.0) (!106)
    + 0.84.0 Notable Changes
      + Fixed: Report parse errors even when invoked with --strict
      + Fixed: SARIF output formatter not handling lists of OWASP or CWE metadata
      + Fixed: Scan yarn.lock dependencies that do not specify a hash
    + 0.83.0 Notable Changes
      + Fixed: Treat Go raw string literals like ordinary string literals
      + Changed: Improved constant propagation for global constants

## v2.18.1
- Update `report` module containing fix for the issue: [#344616](https://gitlab.com/gitlab-org/gitlab/-/issues/344616) (!105)

## v2.18.0
- Update semgrep to [0.82.0](https://github.com/returntocorp/semgrep/releases/tag/v0.82.0) (!98)
    + 0.82.0 Notable Changes
      + Changed: Performance: send all rules directly to semgrep-core instead of invoking semgrep-core
      + Changed: Scans now report a breakdown of how many target paths were skipped for what reason.
      + Changed: Performance: send all rules directly to semgrep-core instead of invoking semgrep-core for each rule, reducing the overhead significantly. Other changes resulting from this: Sarif output now includes all rules run. Error messages use full path of rules. Progress bar reports by file instead of by rule
      + Changed: Bloom filter optimization now considers import module file names, thus speeding up matching of patterns like import { $X } from 'foo'
    + 0.81.0 Notable Changes
      + Fixed: Gracefully handle timeout errors with missing rule_id
    + 0.80.0 Notable Changes
      + Changed: Ruby: a metavariable matching an atom can also be used to match an identifier with the same name
      + Fixed: Handle missing target files without raising an exception
    + 0.79.0 Notable Changes
      + None for GitLab users
    + 0.78.0 Notable Changes
      + Added: Semgrep is now able to symbolically propagate simple definitions. E.g., given an assignment x = foo.bar() followed by a call x.baz(), Semgrep will keep track of x's definition, and it will successfully match x.baz() with a pattern like foo.bar().baz(). This feature should help writing simple yet powerful rules, by letting the dataflow engine take care of any intermediate assignments. Symbolic propagation is still experimental and it is disabled by default, it must be enabled in a per-rule basis using options: and setting symbolic_propagation: true. (#2783, #2859, #3207)
      + Added: metavariable-comparison now handles metavariables that bind to arbitrary constant expressions (instead of just code variables)
      + Fixed: Python: return statement can contain tuple expansions
      + Fixed: metavariable-comparison: do not throw a Not_found exn anymore
      + Fixed: better ordering of match results with respect to captured metavariables
      + Fixed: Go, JavaScript, Java, Python, TypeScript: correct matching of multibyte characters
    + 0.77.0 Notable Changes
      + Fixed: Go: fixed bug where using an ellipsis to stand for a list of key-value pairs would sometimes cause a parse error
      + Fixed: Allow name resolution on imported packages named just vN, where N is a number
      + Fixed: Python: get the correct range when matching comprehension
      + Fixed: Python and other languages: allow matches of patterns containing non-ascii characters, but still with possibly many false positives
      + Changed: Constant propagation is now a proper must-analysis, if a variable is undefined in some path then it will be considered as non-constant
      + Changed: semgrep-core will log a warning when a worker process is consuming above 400 MiB of memory, or reached 80% of the specified memory limit, whatever happens first. This is meant to help diagnosing OOM-related crashes.

## v2.17.0
- Update ruleset, report, and command modules to support ruleset overrides (!102)

## v2.16.1
- Update to gosec rulset v1.0.28 to fix [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/348952) (!96)

## v2.16.0
- Update semgrep to [0.76.2](https://github.com/returntocorp/semgrep/releases/tag/v0.76.2) (!#)
    + 0.76.2 Notable Changes
      + Fixed: Python: set the right scope for comprehension variables
      + Fixed: Fixed bug where the presence of .semgrepignore would cause reported targets to have absolute instead of relative file paths
    + 0.76.1 Notable Changes
      + Fixed: Fixed bug where the presence of .semgrepignore would cause runs to fail on files that were not subpaths of the directory where semgrep was being run
    + 0.76.0 Notable Changes
      + Added: Improved filtering of rules based on file content (important speedup for nodejsscan rules notably)
      + Fixed: TS: parse correctly type definitions
      + Fixed: taint-mode: Findings are now reported when the LHS of an access operator is a sink (e.g. as in $SINK->method), and the LHS operand is a tainted variable
      + Fixed: metavariable-comparison: do not throw a NotHandled exn anymore
      + Fixed: Python: generate proper lexical exn for unbalanced braces
      + Fixed: Python: generate proper lexical exn for unbalanced braces
      + Fixed: Matching "$MVAR" patterns against string literals computed by constant folding no longer causes a crash
      + Changed: semgrep-core: Log messages are now tagged with the process id
      + Changed: Optimization: change bloom filters to use sets, move location of filter
      + Changed: Reduced the size of --debug dumps
      + Changed: Given --output Semgrep will no longer print search results to stdout, but it will only save/post them to the specified file/URL
    + 0.75.0 Notable Changes
      + Fixed: semgrep-ci relies on --disable-nosem still tagging findings with is_ignored correctly. Reverting optimization in 0.74.0 that left this field None when said flag was used
    + 0.74.0 Notable Changes
      + Added: Support for method chaining patterns in Python, Golang, Ruby, and C#, so all GA languages now have method chaining
      + Changed: Constant propagation: Any kind of Python string (raw, byte, or unicode) is now evaluated to a string literal and can be matched by "..."
      + Fixed: Apply generic filters excluding large files and binary files to 'generic' and 'regex' targets as it was already done for the other languages.
      + Fixed: Fix some Stack_overflow when using -filter_irrelevant_rules
    + 0.73.0 Notable Changes
      + Changed: cli: if an invalid config is passed to semgrep, it will fail immediately, even if valid configs are also passed
      + Fixed: Performance: Deduplicate rules by rule-id + behavior so rules are not being run twice
      + Fixed: Catch PCRE errors
      + Fixed: Constant propagation: Avoid "Impossible" errors due to unhandled cases

## v2.15.0
- Update ruleset module to include rule pack synthesis + corresponding test cases/expectations (!93)

## v2.14.0
- Update semgrep to [0.72.0](https://github.com/returntocorp/semgrep/releases/tag/v0.72.0) (!92)
    + 0.72.0 Notable Changes
      + Added: Dataflow: Add partial support for await, yield, &, and other expressions
      + Added: Field-definition-as-assignemnt equivalence that allows matching expression patterns against field definitions. It is disabled by default but can be enabled via rule options: with flddef_assign: true
      + Added: Arrows (a.k.a short lambdas) patterns used to match also regular function definitions. This can now be disabled via rule options: with arrow_is_function: false
      + Added: Javascript variable patterns using the 'var' keyword used to also match variable declarations using 'let' or 'const'. This can now be disabled via rule options: with let_is_var: false
      + Fixed: Constant propagation: In a method call x.f(y), if x is a constant then it will be recognized as such
      + Fixed: Go: match correctly braces in composite literals for autofix
      + Fixed: Go: match correctly parens in cast for autofix
      + Fixed: Go: support ellipsis in return type parameters
      + Fixed: pattern-regex: Hexadecimal notation of Unicode code points is now supported and assumes UTF-8
      + Fixed: pattern-regex: Update documentation, specifying we use PCRE
      + Fixed: metavariable-comparison: if a metavariable binds to a code variable that is known to be constant, then we use that constant value in the comparison
    + 0.71.0 Notable Changes
      + Added: Metavariable equality is enforced across sources/sanitizers/sinks in taint mode, and these metavariables correctly appear in match messages
      + Added: semgrep --validate runs metachecks on the rule
      + Fixed: text_wrapping defaults to MAX_TEXT_WIDTH if get_terminal_size reports width < 1
      + Fixed: Metrics report the error type of semgrep core errors (Timeout, MaxMemory, etc.)
      + Fixed: Prevent bad settings files from crashing Semgrep
      + Fixed: Constant propagation: Tuple/Array destructuring assignments now correctly prevent constant propagation
      + Fixed: JS: Correctly parse metavariables in template strings
      + Fixed: Go: support method interface pattern
      + Changed: Report CI environment variable in metrics for better environment determination
      + Changed: Bash: a simple expression pattern can now match any command argument rather than having to match the whole command
    + 0.70.0 Notable Changes
      + Fixed: Go: support ... in import list, for example import (... "error" ...)

## v2.13.7
- chore: Update go to v1.17 (!91)

## v2.13.6
- fix: Return only on exitcode 4 or 7, rely on sarif module for runtime exceptions (!88)

## v2.13.5
- chore: Use ruleset.TransformToGLSASTReport (!89)

## v2.13.4
- fix: Return non-zero exit codes when executed through entrypoint script (!84)

## v2.13.3
- chore: Use ruleset.ProcessPassthrough helper (!81)

## v2.13.2
- fix: Return non-zero exit codes (!80)
- chore: suppress `--disable-version-check` warning if outdated version (!80)

## v2.13.1
- Update semgrep to [0.69.1](https://github.com/returntocorp/semgrep/releases/tag/v0.69.1) (!83)
    + 0.69.1 Notable Changes
      + Fixed: The --enable-metrics flag is now always a flag, does not optionally take an argument

## v2.13.0
- Update semgrep to [0.69.0](https://github.com/returntocorp/semgrep/releases/tag/v0.69.0) (!82)
    + 0.69.0 Notable Changes
      + Added: C: support ... in parameters and sizeof arguments
      + Added: C: support declaration and function patterns
      + Fixed: Reverted change to exclude minified files from the scan (see changelog for 0.66.0)
      + Fixed: Python: fix range of tuples
      + Fixed: C: fix some wrong typedef inference
      + Fixed: Ruby: put back equivalence on old syntax for keyword arguments
      + Changed: taint-mode: Introduce a new kind of not conflicting sanitizer that must be declared with not_conflicting: true. This affects the change made in 0.68.0 that allowed a sanitizer like - pattern: $F(...) to work, but turned out to affect our ability to specify sanitization by side-effect. Now the default semantics of sanitizers is reverted back to the same as before 0.68.0, and - pattern: $F(...) is supported via the new not-conflicting sanitizers.
    + 0.68.2 Notable Changes
      + Fixed: taint-mode: Fixed (another) bug where a tainted sink could go unreported when the sink is a specific argument in a function call
    + 0.68.1 Notable Changes
      + Added: Added support for raise/throw expressions in the dataflow engine and improved existing support for try-catch-finally
      + Fixed: Respect rule level path filtering
    + 0.68.0 Notable Changes
      + Changed: taint-mode: Sanitizers that match exactly a source or a sink are filtered out, making it possible to use - pattern: $F(...) for declaring that any other function is a sanitizer
      + Changed: taint-mode: Remove built-in source source(...) and built-in sanitizer sanitize(...) used for convenience during early development, this was causing some unexpected behavior in real code that e.g. had a function called source!
      + Changed: Resolution of rulesets (i.e. p/ci) use new rule cdn and do client-side hydration
      + Changed: Set pcre recursion limit so it will not vary with different installations of pcre
      + Changed: Better pcre error handling in semgrep-core
      + Fixed: taint-mode: Fixed bug where a tainted sink could go unreported when the sink is a specific argument in a function call
    + 0.67.0 Notable Changes
      + Added: Added support for break and continue in the dataflow engine
      + Added: Added support for switch statements in the dataflow engine
      + Changed: Taint no longer analyzes dead/unreachable code
      + Changed: Improve error message for segmentation faults/stack overflows
      + Changed: Attribute-expression equivalence that allows matching expression patterns against attributes, it is enabled by default but can be disabled via rule options: with attr_expr: false
      + Fixed: Fix CFG dummy nodes to always connect to exit node
      + Fixed: Deep ellipsis <... x ...> now matches sub-expressions of statements
      + Fixed: Ruby: treat 'foo' as a function call when alone on its line
      + Fixed: Fixed bug in semgrep-core's -filter_irrelevant_rules causing Semgrep to incorrectly skip a file
    + 0.66.0 Notable Changes
      + Changed: Constant propagation now assumes that void methods may update the callee
      + Changed: Various changes to error messages
      + Changed: Minified files are now automatically excluded from the scan, which may result in shorter scanning times for some projects.
      + Fixed: Dataflow: Recognize "concat" method and interpret it in a language-dependent manner

## v2.12.0
- gosec rule-set (!76)

## v2.11.0
- Update semgrep to [0.65.0](https://github.com/returntocorp/semgrep/releases/tag/v0.65.0) (!79)
    + 0.65.0 Notable Changes
      + Fixed: Taint detection with ternary ifs
      + Fixed: Fixed corner-case crash affecting the pattern: $X optimization ("empty And; no positive terms in And")
      + Fixed: Fix semgrep-core crash when a cache file exceeds the file size limit
      + Fixed: Sped up Semgrep interface with tree-sitter parsing
    + 0.64.0 Notable Changes
      + Added: Enable associative matching for string concatenation
      + Changed: Add logging on failure to git ls-files
      + Changed: Ignore files whose contents look minified
      + Changed: Display semgrep-core errors in a better way
      + Fixed: Taint mode will now benefit from semgrep-core's -filter_irrelevant_rules
      + Fixed: Taint mode should no longer report duplicate matches
      + Fixed: Only change source directory when running in docker context
    + 0.63.0 Notable Changes
      + Fixed: Dataflow: Disregard type arguments but not the entire instruction
      + Changed: Optimize ending ... in pattern-insides to simply match anything left
    + 0.62.0 Notable Changes
      + Added: Add help text to sarif formatter output if defined in metadata field.
      + Added: Update shortDescription in sarif formatter output if defined in metadata field.
      + Added: Add tags as defined in metadata field in addition to the existing tags.
      + Fixed: core: Fix parsing of numeric literals in rule files
      + Fixed: Generic mode scanner no longer tries to open submodule folders as files
      + Fixed: pattern-regex with completely empty files
      + Fixed: --sarif exit code with suppressed findings
      + Fixed: Fixed fatal errors when a pattern results in a large number of matches
      + Fixed: Better error message when rule contains empty pattern
      + Changed: Add backtrace to fatal errors reported by semgrep-core
      + Changed: Report errors during rule evaluation to the user
      + Changed: When anded with other patterns, pattern: $X will not be evaluated on its own, but will look at the context and find $X within the metavariables bound, which should be significantly faster
    + 0.61.0 Notable Changes
      + Changed: Run version check and print upgrade message after scan instead of before
      + Changed: Memoize range computation for expressions and speed up taint mode
      + Changed: Report semgrep-core's message upon a parse error
      + Changed: Deprecated the following experimental features:
          + pattern-where-python
          + taint-mode
          + equivalences
          + step-by-step evaluation output
      + Changed: Deduplicate findings that fire on the same line ranges and have the same message.
      + Fixed: core: Do not crash when is not possible to compute range info
      + Fixed: eliminate 6x slowdown when using the '--max-memory' option

## v2.10.1
- update tracking calculator (!78)

## v2.10.0
- flawfinder rule-set (!74)

## v2.9.4
- Update semgrep to [0.60.0](https://github.com/returntocorp/semgrep/releases/tag/v0.60.0) (!72)
    + 0.60.0 Notable Changes
      + Added: Detect duplicate keys in YAML dictionaries in semgrep rules when parsing a rule (e.g., detect multiple 'metavariable' inside one 'metavariable-regex')
      + Fixed: JS: Fixed stack overflows (segmentation faults) when processing very large files
      + Fixed: JS: Detect numeric object keys 1 and 0x1 as equal
      + Fixed: taint-mode: Check nested functions
      + Fixed: taint-mode: foo.x is now detected as tainted if foo is a source of taint
      + Fixed: taint-mode: Do not crash when is not possible to compute range info
      + Changed: Added precise error location for the semgrep metachecker, to detect for example duplicate patterns in a rule
    + 0.59.0 Notable Changes
      + Fixed: Improve location reporting of errors metavariable-pattern: pattern-not-regex now works
      + Fixed: Python: imports are unsugared correctly

## v2.9.3
- Update tracking calculator (!71)

## v2.9.2
- Update semgrep to [0.58.2](https://github.com/returntocorp/semgrep/releases/tag/v0.58.2) (!68)
    + 0.58.2 Notable Changes
      + Fixed: Significant speed improvements, but the binary is now 95MB (from 47MB in 0.58.1, but it was 170MB in 0.58.0)
    + 0.58.1 Notable Changes
      + Changed: Switch from OCaml 4.10.0 to OCaml 4.10.2 (and later to OCaml 4.12.0) resulted in smaller semgrep-core binaries (from 170MB to 47MB) and a smaller docker image (from 95MB to 40MB).
    + 0.58.0 Notable Changes
      + Added: New iteration of taint-mode that allows to specify sources/sanitizers/sinks using arbitrary pattern formulas. This provides plenty of flexibility. Note that we breaks compatibility with the previous taint-mode format, e.g. source(...) must now be written as - pattern: source(...).
      + Added: HTML experimental support. This does not rely on the "generic" mode but instead really parses the HTML using tree-sitter-html. This allows some semantic matching (e.g., matching attributes in any order).
      + Added: Vue.js alpha support
      + Added: New matching option implicit_ellipsis that allows disabling the implicit ... that are added to record patterns, plus allow matching "spread fields" (JS ...x) at any position
      + Added: Support globstar (**) syntax in path include/exclude
      + Fixed: Ruby command shells are distinguished from strings (#3343)
      + Fixed: Java varargs are now correctly matched (#3455)
      + Fixed: Support for partial statements (e.g., try { ... }) for Java (#3417)
      + Fixed: Java generics are now correctly stored in the AST (#3505)
      + Fixed: Constant propagation now works inside Python with statements (#3402)
      + Fixed: Metavariable value replacement in message/autofix no longer mixes up short and long names like $X vs $X2 (#3458)
      + Fixed: Fixed metavariable name collision during interpolation of message / autofix (#3483)
      + Fixed: Revert pattern: $X optimization (#3476)
      + Fixed: metavariable-pattern: Allow filtering using a single pattern or pattern-regex
      + Fixed: Dataflow: Translate call chains into IL
      + Changed: Faster matching times for generic mode

## v2.9.1
- Update semgrep to [0.57.0](https://github.com/returntocorp/semgrep/releases/tag/v0.57.0) (!67)
  + 0.57.0 Changes:
    + Add: new options: field in a YAML rule to enable/disable certain features (e.g., constant propagation).
    + Add: capture groups in pattern-regex: in $1, $2, etc.
    + Add: support metavariables inside atoms (e.g., foo(:$ATOM))
    + Add: support metavariables and ellipsis inside regexp literals (e.g., foo(/.../))
    + Add: associative-commutative matching for bitwise OR, AND, and XOR operations
    + Add: support for $...MVAR in generic patterns.
    + Add: metavariable-pattern: Add support for nested Spacegrep/regex/Comby patterns
    + Add: C#: support ellipsis in method parameters
    + Fixed: C#: parse \_\_makeref, \_\_reftype, \_\_refvalue
    + Fixed: Java: parsing of dots inside function annotations with brackets
    + Fixed: Do not pretend that short-circuit Boolean AND and OR operators are commutative
    + Fixed: metavariable-pattern: Fix crash when nesting a non-generic pattern within a generic rule
    + Fixed: metavariable-pattern: Fix parse info when matching content of a metavariable under a different language
    + Fixed: metavariable-comparison: Fix crash when comparing integers and floats
    + Fixed: generic mode on Markdown files with very long lines will now work
    + Changed: generic mode: files that don't look like nicely-indented programs are no longer ignored, which may cause accidental slowdowns in setups where excessively large files are not excluded explicitly
    + Changed: Do not filter findings with the same range but different metavariable bindings
    + Changed: Set parsing_state.have_timeout when a timeout occurs
    + Changed: Set a timeout of 10s per file
    + Changed: Memoize getting ranges to speed up rules with large ranges
    + Changed: When anded with other patterns, pattern: $X will not be evaluated on its own, but will look at the context and find $X within the metavariables bound, which should be significantly faster
  + 0.56.0 Changes:
    + Add: associative-commutative matching for Boolean AND and OR operations
    + Add: support metavariables inside strings (e.g., foo("$VAR"))
    + Add: support metavariables inside atoms (e.g., foo(:$ATOM))
    + Add: metavariable-pattern: Allow matching the content of a metavariable under a different language.
    + Fixed: C#: Parse attributes for local functions
    + Fixed: Go: Recognize other common package naming conventions
    + Changed: Upgrade TypeScript parser

## v2.9.0
- Add identifier URLs to reports (!65 @mschwager)

## v2.8.2
- Fix False Positive in eslint.detect-non-literal-regexp (!63 @colleend)

## v2.8.1
- Fixed typo in ruleid (!60)

## v2.8.0
- Add [tracking calculator](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator) to semgrep (!56)

## v2.7.0
- Update semgrep to [0.55.1](https://github.com/returntocorp/semgrep/releases/tag/v0.55.1) (!53 @brendongo)
  + Add helpUri to sarif output if rule source metadata is defined
  + Fixed wrong line numbers for multi line generic mode
  + Support ellisis in try-except in Python
  + Run with optimizations on by default
  + Other fixes and additions described in https://github.com/returntocorp/semgrep/releases/tag/v0.55.1
- Use helpUri field in sarif output to attach URL to rule primary identifier

## v2.6.0
- Update Semgrep to [0.54.0](https://github.com/returntocorp/semgrep/releases/tag/v0.54.0) (!48 @mschwager)
  + Changed JSON and SARIF outputs sort keys for predictable results
  + Moved some debug logging to verbose logging
  + Added Per rule parse times and per rule-file parse and match times added to opt-in metrics
  + Some fixes and additions described in https://github.com/returntocorp/semgrep/releases/tag/v0.54.0

## v2.5.1
- Set SEMGREP_USER_AGENT_APPEND to GitLab SAST (!49 @brendongo)

## v2.5.0
- SAST_EXCLUDED_PATHS is passed to semgrep to exclude as semgrep runs (!47)

## v2.4.2
- Add `git` to docker image for Semgrep internals to use (!40 @chmccreery)

## v2.4.1
- Upgrade the `command` package to `v1.1.1` to enable better log messages (!39)

## v2.4.0
- Enable semgrep telemetry (!37)

## v2.3.1
- Fix: Enable bandit rules B301-2/B307 (!34 @underyx)

## v2.3.0
- Speed up eslint.detect-object-injection (!32 @r2c_nathan @mschwager)
- Upgrade semgrep to 0.50.1 (!32)
    + JS/TS: Infer global constants even if the const qualifier is missing (#2978)
    + Support for matching multiple arguments with a metavariable (#3009) This is done with a 'spread metavariable' operator that looks like $...ARGS. This used to be available only for JS/TS and is now available for the other languages (Python, Java, Go, C, Ruby, PHP, and OCaml).
    + JS/TS: Support '...' inside JSX text to match any text, as in <a href="foo">...</a> (#2963)
    + JS/TS: Support metavariables for JSX attribute values, as in <a href=$X>some text</a> (#2964)
    + Python: correctly parsing fstring with multiple colons
    + Remove jsx and tsx from languages, just use javascript or typescript (#3000)
    + Capturing functions when used as both expressions and statements in JS (#1007)
    + Ability to match lambdas or functions in Javascript with ellipsis after the function keyword, (e.g., function ...(...) { ... })
    + support for utf-8 code with non-ascii chars (#2944)
    + JSX/TSX: fixed the range of matched JSX elements (#2685)
    + Javascript: allow ellipsis in arrow body (#2802)
    + Official Python 3.9 support
    + Added basic typed metavariables for javascript and typescript (#2588)
    + ability to process a whole rule in semgrep-core; this will allow whole-rule optimisations and avoid some fork and communication with the semgrep Python wrapper
    + Caching improvements for semgrep-core
    + Matching performance improvements
    + Typescript grammar upgraded
    + Import statements for CommonJS Typescript modules now supported. (#2234)

## v2.2.0
- Support semgrep rule override via the custom ruleset passthrough property (!30)

## v2.1.1
- Fix major version in .gitlab-ci.yml so that the major docker release tag is 2 (!28)

## v2.1.0
- Add `strict`, `no-git-ignore`, and `--no-rewrite-rules` to semgrep flags (!27)

## v2.0.0
- Bump to version 2 so we can support semgrep in the GitLab config UI while we're waiting to remove
  `SAST_ANALYZER_IMAGE_TAG` (!22)

## v0.10.1
- No change patch to allow release pipelines to pass (!26)

## v0.10.0
- Add Description field to vulnerabilities (!24)

## v0.9.0
- Update Dockerfile to support OpenShift (!23)

## v0.8.0
- Add eslint identifiers and update identifier helpers in convert.go (!19)

## v0.7.0
- Update report dependency in order to use the report schema version 14.0.0 (!17)

## v0.6.1
- Add react-dangerouslysetinnerhtml semgrep rule (!16)
- Add detect-non-literal-regexp semgrep rule (!16)
- Add detect-non-literal-fs-filename (!16)
- Add detect-object-injection (!16)

## v0.6.0
- Add eslint and react rule-sets (!12)

## v0.5.0
- Add bandit identifier to `Report.Vulnerabilities` (!10)

## v0.4.0
- Fix paths in report to be relative to project root (!6)
- Update Dockerfile to give us control over base image (!6)
- Update sarif package to support multiple runs and locations (!6)

## v0.3.0
- OWASP metadata added to bandit rule-set (!9)

## v0.2.0
- Bandit rule-set (!2)

## v0.0.1
- First pass at things (!1)
