{
  "version": "15.1.0",
  "vulnerabilities": [
    {
      "id": ":SKIP:",
      "category": "sast",
      "name": "Improper neutralization of directives in dynamically evaluated code ('Eval Injection')",
      "description": "The application was found calling the `eval` function OR Function()\n  constructor OR setTimeout() OR setInterval() methods. If the\n\n  variables or strings or functions passed to these methods contains user-supplied input, an adversary could attempt to execute arbitrary\n\n  JavaScript\n\n  code. This could lead to a full system compromise in Node applications or Cross-site Scripting\n\n  (XSS) in web applications.\n\n\n  To remediate this issue, remove all calls to above methods and consider alternative methods for\n\n  executing\n\n  the necessary business logic. There is almost no safe method of calling `eval` or other above stated sinks with\n\n  user-supplied input.\n\n  Instead, consider alternative methods such as using property accessors to dynamically access\n\n  values.\n\n\n  Example using property accessors to dynamically access an object's property:\n\n  ```\n\n  // Define an object\n\n  const obj = {key1: 'value1', key2: 'value2'};\n\n  // Get key dynamically from user input\n\n  const key = getUserInput();\n\n  // Check if the key exists in our object and return it, or a default empty string\n\n  const value = (obj.hasOwnProperty(key)) ? obj[key] : '';\n\n  // Work with the value\n\n  ```\n\n\n  For more information on why not to use `eval`, and alternatives see:\n\n  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval#never_use_eval!\n\n  Other References:\n\n  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/Function\n\n  - https://developer.mozilla.org/en-US/docs/Web/API/setTimeout\n\n  - https://developer.mozilla.org/en-US/docs/Web/API/setInterval\n",
      "cve": "semgrep_id:eslint.detect-eval-with-expression:47:47",
      "severity": "High",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "src/app.ts",
        "start_line": 47
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "eslint.detect-eval-with-expression",
          "value": "eslint.detect-eval-with-expression",
          "url": "https://semgrep.dev/r/gitlab.eslint.detect-eval-with-expression"
        },
        {
          "type": "cwe",
          "name": "CWE-95",
          "value": "95",
          "url": "https://cwe.mitre.org/data/definitions/95.html"
        },
        {
          "type": "owasp",
          "name": "A03:2021 - Injection",
          "value": "A03:2021"
        },
        {
          "type": "owasp",
          "name": "A1:2017 - Injection",
          "value": "A1:2017"
        },
        {
          "type": "eslint_rule_id",
          "name": "ESLint rule ID/detect-eval-with-expression",
          "value": "detect-eval-with-expression"
        }
      ]
    },
    {
      "id": ":SKIP:",
      "category": "sast",
      "name": "Improper neutralization of directives in dynamically evaluated code ('Eval Injection')",
      "description": "User controlled data in eval() or similar functions may result in Server Side Injection or Remote Code Injection\n",
      "cve": "semgrep_id:nodejs_scan.javascript-eval-rule-eval_nodejs:47:47",
      "severity": "High",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "src/app.ts",
        "start_line": 47
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "nodejs_scan.javascript-eval-rule-eval_nodejs",
          "value": "nodejs_scan.javascript-eval-rule-eval_nodejs"
        },
        {
          "type": "cwe",
          "name": "CWE-95",
          "value": "95",
          "url": "https://cwe.mitre.org/data/definitions/95.html"
        },
        {
          "type": "owasp",
          "name": "A03:2021 - Injection",
          "value": "A03:2021"
        },
        {
          "type": "owasp",
          "name": "A1:2017 - Injection",
          "value": "A1:2017"
        },
        {
          "type": "njsscan_rule_type",
          "name": "NodeJS Scan ID javascript-eval-rule-eval_nodejs",
          "value": "User controlled data in eval() or similar functions may result in Server Side Injection or Remote Code Injection"
        }
      ]
    },
    {
      "id": ":SKIP:",
      "category": "sast",
      "name": "Regular expression with non-literal value",
      "description": "The `RegExp` constructor was called with a non-literal value. If an adversary were able to\nsupply a malicious regex, they could cause a Regular Expression Denial of Service (ReDoS)\nagainst the application. In Node applications, this could cause the entire application to no\nlonger be responsive to other users' requests.\n\nTo remediate this issue, never allow user-supplied regular expressions. Instead, the regular \nexpression should be  hardcoded. If this is not possible, consider using an alternative regular\nexpression engine such as [node-re2](https://www.npmjs.com/package/re2). RE2 is a safe alternative \nthat does not support backtracking, which is what leads to ReDoS.\n\nExample using re2 which does not support backtracking (Note: it is still recommended to\nnever use user-supplied input):\n```\n// Import the re2 module\nconst RE2 = require('re2');\n\nfunction match(userSuppliedRegex, userInput) {\n    // Create a RE2 object with the user supplied regex, this is relatively safe\n    // due to RE2 not supporting backtracking which can be abused to cause long running\n    // queries\n    var re = new RE2(userSuppliedRegex);\n    // Execute the regular expression against some userInput\n    var result = re.exec(userInput);\n    // Work with the result\n}\n```\n\nFor more information on Regular Expression DoS see:\n- https://owasp.org/www-community/attacks/Regular_expression_Denial_of_Service_-_ReDoS\n",
      "cve": "semgrep_id:eslint.detect-non-literal-regexp:34:34",
      "severity": "Medium",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "src/app.ts",
        "start_line": 34
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "eslint.detect-non-literal-regexp",
          "value": "eslint.detect-non-literal-regexp",
          "url": "https://semgrep.dev/r/gitlab.eslint.detect-non-literal-regexp"
        },
        {
          "type": "cwe",
          "name": "CWE-185",
          "value": "185",
          "url": "https://cwe.mitre.org/data/definitions/185.html"
        },
        {
          "type": "owasp",
          "name": "A03:2021 - Injection",
          "value": "A03:2021"
        },
        {
          "type": "owasp",
          "name": "A1:2017 - Injection",
          "value": "A1:2017"
        },
        {
          "type": "eslint_rule_id",
          "name": "ESLint rule ID/detect-non-literal-regexp",
          "value": "detect-non-literal-regexp"
        }
      ]
    },
    {
      "id": ":SKIP:",
      "category": "sast",
      "name": "Use of cryptographically weak pseudo-random number generator (PRNG)",
      "description": "Depending on the context, generating weak random numbers may expose cryptographic functions,\nwhich rely on these numbers, to be exploitable. When generating numbers for sensitive values\nsuch as tokens, nonces, and cryptographic keys, it is recommended that the `randomBytes` method\nof the `crypto` module be used instead of `pseudoRandomBytes`.\n\nExample using `randomBytes`:\n```\n// Generate 256 bytes of random data\nconst randomBytes = crypto.randomBytes(256);\n```\n\nFor more information on JavaScript Cryptography see:\nhttps://nodejs.org/api/crypto.html#cryptorandombytessize-callback\n",
      "cve": "semgrep_id:eslint.detect-pseudoRandomBytes:41:41",
      "severity": "Medium",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "src/app.ts",
        "start_line": 41
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "eslint.detect-pseudoRandomBytes",
          "value": "eslint.detect-pseudoRandomBytes",
          "url": "https://semgrep.dev/r/gitlab.eslint.detect-pseudoRandomBytes"
        },
        {
          "type": "cwe",
          "name": "CWE-338",
          "value": "338",
          "url": "https://cwe.mitre.org/data/definitions/338.html"
        },
        {
          "type": "owasp",
          "name": "A02:2021 - Cryptographic Failures",
          "value": "A02:2021"
        },
        {
          "type": "owasp",
          "name": "A3:2017 - Sensitive Data Exposure",
          "value": "A3:2017"
        },
        {
          "type": "eslint_rule_id",
          "name": "ESLint rule ID/detect-pseudoRandomBytes",
          "value": "detect-pseudoRandomBytes"
        }
      ]
    },
    {
      "id": ":SKIP:",
      "category": "sast",
      "name": "Improper neutralization of input during web page generation ('Cross-site Scripting')",
      "description": "This application accepts user input directly from the client side without validation.  This could lead to Cross Site Scripting (XSS) if the input contains malicious script code and  the application server does not properly escape or sanitize the output.   Consider encoding input data before sending it to the client side.  \n``` // safe method of sending user input data router.get('/safe/1', (req, res) => {\n  var name = encodeURI(req.query.name); \n  res.send(name);\n}) ```\nXSS is an attack that exploits a web application or system to treat user input as markup or script code.  It is important to encode the data depending on the specific context in which it is used. \n",
      "cve": "semgrep_id:nodejs_scan.javascript-xss-rule-express_xss:36:36",
      "severity": "Medium",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "src/app.ts",
        "start_line": 36
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "nodejs_scan.javascript-xss-rule-express_xss",
          "value": "nodejs_scan.javascript-xss-rule-express_xss"
        },
        {
          "type": "cwe",
          "name": "CWE-79",
          "value": "79",
          "url": "https://cwe.mitre.org/data/definitions/79.html"
        },
        {
          "type": "owasp",
          "name": "A03:2021 - Injection",
          "value": "A03:2021"
        },
        {
          "type": "owasp",
          "name": "A7:2017 - Cross-Site Scripting (XSS)",
          "value": "A7:2017"
        },
        {
          "type": "njsscan_rule_type",
          "name": "NodeJS Scan ID javascript-xss-rule-express_xss",
          "value": "Untrusted User Input in Response will result in Reflected Cross Site Scripting Vulnerability."
        }
      ]
    }
  ],
  "dependency_files": null,
  "scan": {
    "analyzer": {
      "id": "semgrep",
      "name": "Semgrep",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/semgrep",
      "vendor": {
        "name": "GitLab"
      },
      "version": ":SKIP:"
    },
    "scanner": {
      "id": "semgrep",
      "name": "Semgrep",
      "url": "https://github.com/returntocorp/semgrep",
      "vendor": {
        "name": "GitLab"
      },
      "version": ":SKIP:"
    },
    "type": "sast",
    "start_time": ":SKIP:",
    "end_time": ":SKIP:",
    "status": "success"
  }
}
