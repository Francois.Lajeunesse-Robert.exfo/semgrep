# Semgrep analyzer

The Semgrep analyzer performs SAST scanning on repositories containing code written in several languages:

* C# (.NET)
* C
* C++
* Go
* Kotlin
* Java
* JavaScript
* Objective-C
* PHP
* Python
* Ruby
* Rust
* Scala
* Swift
* TypeScript

The analyzer wraps [Semgrep](https://github.com/returntocorp/semgrep), and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## SAST Rules

The [`sast-rules`](https://gitlab.com/gitlab-org/security-products/sast-rules) repository is the source of truth for the GitLab Semgrep rulesets. Changes to rules should be made in `sast-rules`. A CI job is responsible for validating and publishing the latest rules, which will eventually be consumed by the Semgrep analyzer here.

### Auto-Resolution of Removed SAST Rules

When a new version of `semgrep` is released that omits certain rules from the previous version, any findings linked to those excluded rules are automatically resolved upon running the new version. This process is documented in the [Gitlab Docs](https://docs.gitlab.com/ee/user/application_security/sast/#automatic-vulnerability-resolution).

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Dismissing a Failing BAP Tests

The [Batch Analysis Platform](https://gitlab.com/gitlab-org/secure/tools/bap) (aka `BAP`) is a service we execute as part of our [CI pipeline](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/blob/b2b491948972a58eed21178a1e3bd312ccf03148/.gitlab-ci.yml#L55). `BAP` adds an extra layer of confidence that changes to the [Semgrep](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep) analyzer do not inadvertently alter its ability to detect vulnerabilities. However, some expected changes, such as the removal of a rule with a high false positive rate, can cause the BAP CI job to fail.

To handle an expected failure, follow these steps:

1. **Locate the BAP Report:** The failing BAP report can be found in the artifacts of the [bap-analysis-verify-analysis](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/blob/v5.2.3/.gitlab-ci.yml#L108-125) job.
1. **Comment on the Merge Request:** Provide an explanation for the failure. Ensure your comment includes a link to the failing BAP report. For example [MR BAP note](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/merge_requests/397#note_1863854285).
1. **Disable BAP and rerun the Pipeline:** Re-run the MR's pipeline with the environment variable `BAP_DISABLED` set to `true`.

By following these steps, you can effectively manage and dismiss expected BAP test failures while maintaining transparency and traceability.

## Failing Integation Tests

Whenever `SAST_RULES_VERSION` in `Dockerfile` and `Dockerfile.fips` gets
changed, it is likely that the corresponding integration tests are failing.
This project leverages the
[integration-test](https://gitlab.com/gitlab-org/security-products/analyzers/integration-test)
tool to perform integration tests; it uses [RSpec](https://rspec.info/) to
implement the integration test logic. In essence, `integration-test` launches
the semgrep docker image and runs the `./spec/semgrep_image_spec.rb` file that
runs semgrep (within a Docker container) on a set of test files located in
`qa/fixtures`, generates reports and compares them against the test
expectations ins `qa/expect`. The output of `integration-test` can be very
verbose even for small deviations from the expectations. In the presence of an
error, it is advised to scroll to the bottom of the job log where you find a
concise description of the failing test cases.

In addition to the log analysis, it can be very helpful to generate the test
expectations to understand the impact of rule changes. For regenerating test
expectations, you can use the script [analyzer-refresh-expected-json](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/fbf7a20829108579fa8e861412403e019701e646/analyzer-refresh-expected-json) where `TMP_IMAGE` can be
replaced with the URL that points to the latest image that was produced in the
`build tmp image` job. The environment variable setting `REFRESH_EXPECTED=true`
instructs the `integration-test` tool to regenerate the expectations so that
they pass the integration test jobs.

After running the script [analyzer-refresh-expected-json](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/fbf7a20829108579fa8e861412403e019701e646/analyzer-refresh-expected-json) in the git root directory of semgrep,
you should see the updated test expectation files in your repository. You can
then use standard tools such as `git diff` to better understand the changes. In
the diff you may come across `:SKIP:` placeholders which are used to ignore
certain fields from the baseline comparison. It is advised to use them for
fields that are irrelevant with regards to the integration test such as the
`id`, `version`, `start_time` and `end_time` fields for vulnerabilites.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
